# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.3.1] - 2020-07-11
### Changed
- Many small cleanups

## [0.3.0] - 2020-01-30
### Removed
- `with_capacity_from`, it was problematic. May consider adding back in future.

### Added
- `capacity()`, return the backing capacity of the store
- `len()`, get the actual used length

### Changed
Many internal changes to speed things up as much as possible. This does however, mean
that there is a lot more use of `unsafe`. There is only so much you can do with 
completely `safe` Rust.


## [0.2.3] - 2020-01-29
### Added
- Begin changelog
- drone CI
- Gitlab CI
