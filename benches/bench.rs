#![allow(unused)]
use bencher::{benchmark_group, benchmark_main, black_box, Bencher};
use persist_o_vec::*;

const LIMIT: usize = 10_000;

struct Velocity {
    x: f64,
    y: f32,
    d: u8,
}

fn populate() -> Persist<Velocity> {
    let mut persist = Persist::with_capacity(LIMIT);
    for _ in 0..LIMIT {
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
    }
    persist
}

fn pst_init_with_capacity(b: &mut Bencher) {
    b.iter(|| {
        let x: Persist<u32> = Persist::with_capacity(LIMIT);
        black_box(x);
    })
}

fn pst_populate_to_capacity(b: &mut Bencher) {
    let mut persist = Persist::with_capacity(LIMIT);
    b.iter(|| {
        persist = populate();
        black_box(persist.clear());
    })
}

fn pst_populate_to_capacity_fast(b: &mut Bencher) {
    let mut persist = Persist::with_capacity(LIMIT);
    b.iter(|| {
        persist = Persist::with_capacity_filled_by(LIMIT, || Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        black_box(persist.clear());
    })
}

fn pst_iter(b: &mut Bencher) {
    let persist = populate();
    b.iter(|| {
        for x in persist.iter() {
            black_box(x.x);
        }
    });
}

fn pst_iter_mut(b: &mut Bencher) {
    let mut persist = populate();
    b.iter(|| {
        for x in persist.iter_mut() {
            black_box(x.x += 1.0);
        }
    });
}

fn pst_remove_and_push(b: &mut Bencher) {
    let mut persist = populate();
    let mut z = 0.0;
    b.iter(|| {
        let x = persist.remove(10).unwrap();
        z += x.x;
        let x = persist.remove(50).unwrap();
        z += x.x;
        let x = persist.remove(100).unwrap();
        z += x.x;

        let x = persist.remove(1500).unwrap();
        z += x.x;
        let x = persist.remove(200).unwrap();
        z += x.x;
        let x = persist.remove(2500).unwrap();
        z += x.x;
        let x = persist.remove(300).unwrap();
        z += x.x;
        let x = persist.remove(3500).unwrap();
        z += x.x;
        let x = persist.remove(4_000).unwrap();
        z += x.x;
        let x = persist.remove(5_000).unwrap();
        z += x.x;
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });

        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        persist.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        black_box(z);
    });
}

fn vec_iter(b: &mut Bencher) {
    let mut vec = Vec::with_capacity(LIMIT);
    for _i in 0..LIMIT {
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
    }
    b.iter(|| {
        for x in vec.iter() {
            black_box(x.x);
        }
    });
}

fn vec_iter_mut(b: &mut Bencher) {
    let mut vec = Vec::with_capacity(LIMIT);
    for _ in 0..LIMIT {
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
    }
    b.iter(|| {
        for x in vec.iter_mut() {
            x.x += 1.0;
            black_box(x);
        }
    });
}

fn vec_populate_to_capacity(b: &mut Bencher) {
    let mut vec = Vec::new();
    b.iter(|| {
        vec = Vec::with_capacity(LIMIT);
        for _ in 0..LIMIT {
            black_box(vec.push(Velocity {
                x: 1.0,
                y: 2.0,
                d: 1,
            }));
        }
    })
}

fn vec_remove_and_push(b: &mut Bencher) {
    let mut vec = Vec::with_capacity(LIMIT);
    for _ in 0..LIMIT {
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
    }
    let mut z = 0.0;
    b.iter(|| {
        let x = vec.remove(10);
        z += x.x;
        let x = vec.remove(50);
        z += x.x;
        let x = vec.remove(100);
        z += x.x;

        let x = vec.remove(1500);
        z += x.x;
        let x = vec.remove(200);
        z += x.x;
        let x = vec.remove(2500);
        z += x.x;
        let x = vec.remove(300);
        z += x.x;
        let x = vec.remove(3500);
        z += x.x;
        let x = vec.remove(4_000);
        z += x.x;
        let x = vec.remove(5_000);
        z += x.x;
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });

        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        vec.push(Velocity {
            x: 1.0,
            y: 2.0,
            d: 1,
        });
        black_box(z);
    });
}

benchmark_group!(
    benches,
    pst_init_with_capacity,
    pst_populate_to_capacity,
    pst_populate_to_capacity_fast,
    vec_populate_to_capacity,
    pst_iter,
    vec_iter,
    pst_iter_mut,
    vec_iter_mut,
    pst_remove_and_push,
    vec_remove_and_push,
);
benchmark_main!(benches);
